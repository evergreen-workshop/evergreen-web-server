package net.evergreenworkshop.form.web;

import net.evergreenworkshop.form.model.Form;
import net.evergreenworkshop.form.service.SendingMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.Logger;

@CrossOrigin(origins = "http://localhost:8000")
@RestController
public class FormController {
    @Autowired
    SendingMailService sendingMailService;

    @PostMapping(path = "/form")
    public ResponseEntity formPost(@RequestBody Form formData) {
        try {
            String subject = formData.getName() + " - " + formData.getEmail() + " sent you a service inquiry.";
            sendingMailService.sendMail(subject, formData.getServices() + formData.getMessage());
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }
    }
}
