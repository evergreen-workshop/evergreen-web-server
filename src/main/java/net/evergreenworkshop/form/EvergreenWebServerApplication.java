package net.evergreenworkshop.form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvergreenWebServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvergreenWebServerApplication.class, args);
	}

}
