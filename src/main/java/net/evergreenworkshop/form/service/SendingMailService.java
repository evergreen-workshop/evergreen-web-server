package net.evergreenworkshop.form.service;

public interface SendingMailService {
    boolean sendMail(String subject, String body);
}
